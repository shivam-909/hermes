package main

import (
	"context"
	"log"
	"time"

	"github.com/Shopify/sarama"
	"golang.org/x/sync/errgroup"

	"gitlab.com/shivam-909/heck"
	"gitlab.com/shivam-909/hermes"
)

func run_consumer() {

	c, err := loggerConsumer(newConsumerConfig())
	if err != nil {
		log.Fatalf("failed to create logger consumer")
	}

	eg := errgroup.Group{}

	ctx := context.Background()

	eg.Go(func() error {
		return c.StartListening(ctx)
	})

	if err := eg.Wait(); err != nil {
		log.Fatalf(err.Error())
	}
}

func loggerConsumer(cfg *hermes.ConsumerConfig) (*hermes.Consumer, error) {

	f := func(
		session sarama.ConsumerGroupSession,
		claim sarama.ConsumerGroupClaim,
	) error {
		for {
			select {
			case message := <-claim.Messages():
				log.Printf("message received at : %v, value : %v", message.Timestamp.Format(time.RFC3339), string(message.Value))
				session.MarkMessage(message, "")

			case <-session.Context().Done():
				return nil
			}
		}
	}

	c, err := hermes.NewConsumer(cfg, f)
	if err != nil {
		return nil, heck.Wrap("failed to create new consumer: %v", err)
	}

	return c, nil
}
