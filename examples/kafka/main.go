package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"gitlab.com/shivam-909/hermes"
)

var (
	username       = flag.String("username", "", "username")
	password       = flag.String("password", "", "password")
	brokers        = flag.String("brokers", "localhost:9092", "addresses of kafka brokers separated by a comma")
	topics         = flag.String("topics", "", "topics to consume separated by a comma")
	consumer_group = "example_consumer_group"
	assignor       = "range"
)

func newConsumerConfig() *hermes.ConsumerConfig {
	return &hermes.ConsumerConfig{
		Brokers:  strings.Split(*brokers, ","),
		Topics:   strings.Split(*topics, ","),
		Group:    consumer_group,
		Username: *username,
		Password: *password,
		Assignor: assignor,
	}
}

func newProducerConfig() *hermes.ProducerConfig {
	return &hermes.ProducerConfig{
		Brokers:  strings.Split(*brokers, ","),
		Username: *username,
		Password: *password,
	}
}

func main() {
	hermes.SetVerbosity(false)
	flag.Parse()
	c := make(chan os.Signal, 1)
	defer close(c)

	signal.Notify(c, syscall.SIGINT)

	go run_consumer()
	go run_producer()

	<-c
	log.Println("terminating program...")
}
