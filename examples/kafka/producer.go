package main

import (
	"fmt"
	"log"
	"time"

	"github.com/Shopify/sarama"
	"gitlab.com/shivam-909/hermes"
)

func run_producer() {
	p, err := hermes.NewProducer(newProducerConfig())
	if err != nil {
		log.Fatal(err)
	}

	go p.StartListening()

	duration := 5 * time.Second
	ticker := time.NewTicker(duration)

	for {
		select {
		case t := <-ticker.C:
			p.Messages() <- &sarama.ProducerMessage{
				Topic: "example",
				Key:   sarama.StringEncoder("example-key"),
				Value: sarama.StringEncoder(fmt.Sprintf("message produced at %s", t.Format(time.RFC3339))),
			}
		}
	}
}
