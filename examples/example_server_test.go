package main

import (
	"context"
	"fmt"

	"github.com/bufbuild/connect-go"
	"gitlab.com/shivam-909/hermes"
	"gitlab.com/shivam-909/hermes/exampleproto"
	"gitlab.com/shivam-909/hermes/exampleproto/exampleprotoconnect"
)

type HelloServer struct{}

func (s *HelloServer) Hello(
	ctx context.Context,
	req *connect.Request[exampleproto.Empty],
) (*connect.Response[exampleproto.HelloResponse], error) {
	return connect.NewResponse(&exampleproto.HelloResponse{
		Greeting: "Hello, World!",
	}), nil
}

func HelloWorld(req *hermes.Request) hermes.Response {
	return req.Respond("Hello, World!", 200)
}

func HelloWorldNested(req *hermes.Request) hermes.Response {
	res := fmt.Sprintf("Hello, World! Method: %v", string(req.Header.Method()))
	return req.Respond(res, 200)
}

func main() {
	server := hermes.NewServer()
	router := &hermes.Router{}

	router.GET("/", HelloWorld)
	helloGroup := router.Group("/group")

	helloGroup.GET("/get", HelloWorldNested)
	helloGroup.POST("/post", HelloWorldNested)
	helloGroup.Register("*", "/", HelloWorldNested)

	hello := &HelloServer{}
	path, handler := exampleprotoconnect.NewExampleServiceHandler(hello)

	server.RegisterRPC(path, handler)

	server.Serve(router.Serve(), "localhost:8080", "localhost:8081")
}
