package hermes

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/valyala/fasthttp"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

type Server struct {
	srv    *fasthttp.Server
	srvrpc *http.Server
	mux    *http.ServeMux

	shuttingDown chan struct{}

	shutOnce    sync.Once
	onShutdown  []func(context.Context)
	onShutdownM sync.Mutex
}

func NewServer() *Server {
	srv := &fasthttp.Server{}
	srvrpc := &http.Server{}
	mux := http.NewServeMux()
	return &Server{
		srv:    srv,
		srvrpc: srvrpc,
		mux:    mux,

		shutOnce: sync.Once{},

		shuttingDown: make(chan struct{}),
		onShutdown:   make([]func(context.Context), 0),
		onShutdownM:  sync.Mutex{},
	}
}

func (s *Server) RegisterShutdownFunc(fs ...func(context.Context)) {
	s.onShutdown = append(s.onShutdown, fs...)
}

func (s *Server) Stop(ctx context.Context) {
	s.onShutdownM.Lock()
	defer s.onShutdownM.Unlock()
	s.shutOnce.Do(func() {
		close(s.shuttingDown)
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			defer wg.Done()
			if err := s.srv.Shutdown(); err != nil {
				log.Printf("failed forceful shutdown. exiting %v", err)
				panic(err)
			}
		}()
		for _, f := range s.onShutdown {
			f := f
			wg.Add(1)
			go func() {
				defer wg.Done()
				f(ctx)
			}()
		}
		wg.Wait()
	})
}

type ServeOptions struct {
	addrHttp string
	addrgRPC string
}

func DefaultServeOptions() *ServeOptions {
	return &ServeOptions{
		addrHttp: "localhost:8080",
		addrgRPC: "localhost:8081",
	}
}

// Serves both HTTP and gRPC
func (s *Server) Serve(svc Service, opt *ServeOptions) {
	h := func(ctx *fasthttp.RequestCtx) {
		request := &Request{
			Request: ctx.Request,
			ctx:     ctx,
		}

		res := svc(request)

		ctx.SetBody(res.Body())
		ctx.Response.Header = res.Header
		ctx.SetContentType("application/json")
	}

	s.srv.Handler = h

	if opt == nil {
		opt = DefaultServeOptions()
	}

	s.srvrpc.Addr = opt.addrgRPC
	s.srvrpc.Handler = h2c.NewHandler(s.mux, &http2.Server{})

	if opt.addrHttp != "" {
		go s.srv.ListenAndServe(opt.addrHttp)
		log.Printf("serving http on :: %v \n", opt.addrHttp)
	}

	if opt.addrgRPC != "" {
		go s.srvrpc.ListenAndServe()
		log.Printf("serving grpc on :: %v \n", s.srvrpc.Addr)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM)

	<-done

	log.Printf("shutting down")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	s.Stop(ctx)
	s.srvrpc.Shutdown(ctx)
}

func (s *Server) RegisterRPC(path string, handler http.Handler) {
	s.mux.Handle(path, handler)
}
