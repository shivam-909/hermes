package hermes

import (
	"context"

	"github.com/valyala/fasthttp"
)

type Request struct {
	fasthttp.Request

	err error // middleware may choose to set an error
	ctx *fasthttp.RequestCtx
}

func (r *Request) Ctx() context.Context {
	return r.ctx
}

func (r *Request) SetCtx(ctx context.Context) {
	r.ctx = &fasthttp.RequestCtx{}
}

func (r *Request) Err() error {
	return r.err
}

func (r *Request) SetErr(err error) {
	r.err = err
}

func (r *Request) Respond(v interface{}, code int) Response {
	return NewResponseWithCode(r, v, code)
}
