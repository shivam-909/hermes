package hermes

import (
	"errors"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/Shopify/sarama"
	"gitlab.com/shivam-909/heck"
)

type Producer struct {
	producer sarama.AsyncProducer
	messages chan *sarama.ProducerMessage
}

func (p *Producer) Messages() chan *sarama.ProducerMessage {
	return p.messages
}

type ProducerConfig struct {
	Username string
	Password string
	Brokers  []string
	scfg     *sarama.Config
}

func (c *ProducerConfig) Sarama() *sarama.Config {
	return c.scfg
}

func (c *ProducerConfig) valid() bool {
	if len(c.Brokers) == 0 {
		return false
	}

	if c.Username == "" || c.Password == "" {
		return false
	}

	return true
}

func NewProducer(cfg *ProducerConfig) (*Producer, error) {
	if !cfg.valid() {
		return nil, errors.New("invalid configuration")
	}

	saramaConfig := DefaultSaramaConfig(cfg.Username, cfg.Password)

	producer, err := sarama.NewAsyncProducer(cfg.Brokers, saramaConfig)
	if err != nil {
		return nil, heck.Wrap("failed to create async producer : %v", err)
	}

	return &Producer{
		producer: producer,
		messages: make(chan *sarama.ProducerMessage),
	}, nil
}

func (p *Producer) StartListening() error {
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)
	defer p.producer.AsyncClose()

	for {
		select {
		case m := <-p.messages:
			m.Timestamp = time.Now()
			p.producer.Input() <- m
		case <-signals:
			log.Println("terminating the program")
			return nil
		}
	}
}
