module gitlab.com/shivam-909/hermes

go 1.18

require github.com/valyala/fasthttp v1.38.0

require (
	github.com/Shopify/sarama v1.36.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/eapache/go-resiliency v1.3.0 // indirect
	github.com/eapache/go-xerial-snappy v0.0.0-20180814174437-776d5712da21 // indirect
	github.com/eapache/queue v1.1.0 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/hashicorp/go-uuid v1.0.3 // indirect
	github.com/jcmturner/aescts/v2 v2.0.0 // indirect
	github.com/jcmturner/dnsutils/v2 v2.0.0 // indirect
	github.com/jcmturner/gofork v1.7.6 // indirect
	github.com/jcmturner/gokrb5/v8 v8.4.3 // indirect
	github.com/jcmturner/rpc/v2 v2.0.3 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
	github.com/qiangxue/fasthttp-routing v0.0.0-20160225050629-6ccdc2a18d87 // indirect
	github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475 // indirect
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa // indirect
	golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4 // indirect
	golang.org/x/text v0.3.7 // indirect
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/bufbuild/connect-go v0.3.0
	github.com/cassini-engineering/wtf v0.0.0-20220717174648-a3af3ceffcd1
	github.com/google/uuid v1.3.0
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	gitlab.com/shivam-909/heck v0.0.0-20220813143030-7478a155fc4b
	golang.org/x/net v0.0.0-20220809184613-07c6da5e1ced
	google.golang.org/protobuf v1.28.1
)
