package hermes

import (
	"context"

	"github.com/google/uuid"
)

type Service func(req *Request) Response

func (s Service) Filter(f Filter) Service {
	return func(req *Request) Response {
		return f(s, req)
	}
}

type Filter func(svc Service, req *Request) Response

type AuthenticatorFunc func(ctx context.Context, req *Request) (interface{}, error)
type AuthenticatorCallback func(data interface{}, req *Request)

// FilterAuthenticateAndSetUserData returns a filter function that takes in an authenticator func and a callback func.
// The authenticator should use the authenticate the request, and either return nil interface to return a 403, or an error
// to return a 500.
func FilterAuthenticate(a AuthenticatorFunc, c AuthenticatorCallback) Filter {
	return func(svc Service, req *Request) Response {
		ud, err := a(req.Ctx(), req)
		if err != nil {
			return NewResponseWithCode(req, nil, 500)
		}

		if ud == nil {
			return NewResponseWithCode(req, nil, 403)
		}

		c(ud, req)

		return svc(req)
	}
}

// CallbackSetUserID is a generic callback func that assumes the data provided is a string, and sets
// that string to a UserID header.
func CallbackSetUserID(data interface{}, req *Request) {
	id, ok := data.(string)
	if !ok {
		return
	}

	req.Header.Set("UserID", id)
}

func FilterAuthenticateAndSetUserID(a AuthenticatorFunc) Filter {
	return FilterAuthenticate(a, CallbackSetUserID)
}

func FilterAttachTraceID(svc Service, req *Request) Response {
	id := uuid.New().String()

	req.Header.Set("Trace", id)

	return svc(req)
}
