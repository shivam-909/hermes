package hermes

import (
	"encoding/json"
	"errors"
	"strings"

	"github.com/bufbuild/connect-go"
	"github.com/cassini-engineering/wtf"
	"github.com/valyala/fasthttp"
)

type Response struct {
	fasthttp.Response

	err     error
	Request *Request
}

func NewResponse(req *Request, b interface{}) Response {
	return NewResponseWithCode(req, b, 200)
}

func NewResponseWithCode(req *Request, b interface{}, code int) Response {
	r := &Response{
		Request: req,
	}
	r.Encode(b)
	r.SetStatusCode(code)

	return *r
}

func (r *Response) Encode(v interface{}) {
	switch b := v.(type) {
	case *connect.Request[any]:
		r.FromConnect(b)
	default:
		r.JSON(v)
	}
}

func (r *Response) JSON(v interface{}) {
	if v == nil {
		r.SetBody(nil)
	}
	b, err := json.Marshal(v)
	if err != nil {
		r.err = wtf.Wrap("failed to encode response as json", err)
	}
	r.SetBody(b)
}

func (r *Response) ToConnect(v *connect.Request[any]) error {
	return json.Unmarshal(r.Body(), v)
}

func (r *Response) FromConnect(v *connect.Request[any]) {
	for k, v := range v.Header() {
		vals := strings.Join(v, " ")
		r.Header.Set(k, vals)
	}

	r.JSON(v)
}

func (r *Response) Decode(v interface{}) error {
	if r.err != nil {
		if r.Request != nil {
			return wtf.Wrap("downstream error: %v", r.err)
		}
		return r.err
	}

	if r.Body() == nil {
		return errors.New("response has no body")
	}

	switch v := v.(type) {
	case *connect.Request[any]:
		r.ToConnect(v)
		return nil
	default:
		return json.Unmarshal(r.Body(), v)
	}

}
