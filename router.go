package hermes

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

// A lot of this stuff is derived from monzo/typhon

var (
	routerComponentsRe = regexp.MustCompile(`(?:^|/)(\*\w*|:\w+)`)
)

type entry struct {
	Method  string
	Pattern string
	Service Service
	re      *regexp.Regexp
}

func (e entry) String() string {
	return fmt.Sprintf("%s %s", e.Method, e.Pattern)
}

type Router struct {
	entries []entry
	groups  []*RouterGroup
}

func (r *Router) compile(pattern string) *regexp.Regexp {
	re, pos := ``, 0
	for _, m := range routerComponentsRe.FindAllStringSubmatchIndex(pattern, -1) {
		re += regexp.QuoteMeta(pattern[pos:m[2]])
		token := pattern[m[2]:m[3]]
		switch sigil, name := token[0], token[1:]; sigil {
		case '*':
			if len(name) == 0 {
				re += `.*?`
			} else {
				re += `(?P<` + name + `>.*?)`
			}
		case ':':
			re += `(?P<` + name + `>[^/]+)`
		default:
			panic(fmt.Errorf("unhandled router token %#v", token))
		}
		pos = m[3]
	}
	re += regexp.QuoteMeta(pattern[pos:]) // tail
	re = `^` + re + `$`
	return regexp.MustCompile(re)
}

func (r *Router) Register(method, pattern string, svc Service) {
	re := r.compile(pattern)
	r.entries = append(r.entries, entry{
		Method:  strings.ToUpper(method),
		Pattern: pattern,
		Service: svc,
		re:      re})
}

func (r Router) lookup(method, path string, params map[string]string) (Service, string, bool) {
	method = strings.ToUpper(method)

	for i := len(r.groups) - 1; i >= 0; i-- {
		g := r.groups[i]
		if g.re.MatchString(PathBase(path)) {
			cut := strings.Split(path, "/")
			nested := "/" + strings.Join(cut[2:], "/")
			return g.router.lookup(method, nested, params)
		}
	}

	for i := len(r.entries) - 1; i >= 0; i-- {
		e := r.entries[i]
		if (e.Method == method || e.Method == `*`) && e.re.MatchString(path) {
			// We have a match
			if params != nil && e.re.NumSubexp() > 0 {
				names := e.re.SubexpNames()[1:]
				for i, value := range e.re.FindStringSubmatch(path)[1:] {
					params[names[i]] = value
				}
			}
			return e.Service, e.Pattern, true
		}
	}
	return nil, "", false
}

func (r Router) Lookup(method, path string) (Service, string, map[string]string, bool) {
	params := map[string]string{}
	svc, pattern, ok := r.lookup(method, path, params)
	return svc, pattern, params, ok
}

func (r Router) Serve() Service {
	return func(req *Request) Response {
		method, path := string(req.Request.Header.Method()), string(req.Request.URI().Path())
		svc, _, ok := r.lookup(method, path, nil)
		if !ok {
			txt := fmt.Sprintf("No handler for %s %s", method, path)
			rsp := NewResponse(req, txt)
			rsp.err = errors.New("no handler registered")
			return rsp
		}
		rsp := svc(req)
		if rsp.Request == nil {
			rsp.Request = req
		}
		return rsp
	}
}
func (r Router) Pattern(req Request) string {
	method, path := string(req.Request.Header.Method()), string(req.Request.URI().Path())

	_, pattern, _ := r.lookup(method, path, nil)
	return pattern
}
func (r Router) Params(req Request) map[string]string {
	method, path := string(req.Request.Header.Method()), string(req.Request.URI().Path())

	_, _, params, _ := r.Lookup(method, path)
	return params
}

func PathBase(path string) string {
	spl := strings.Split(path, "/")
	return "/" + spl[1]
}

type RouterGroup struct {
	Pattern string
	re      *regexp.Regexp
	router  *Router
}

func (r *Router) Group(pattern string) *RouterGroup {
	re := r.compile(pattern)
	new := &Router{}
	g := &RouterGroup{
		Pattern: pattern,
		re:      re,
		router:  new,
	}
	r.groups = append(r.groups, g)
	return g
}

func (r *RouterGroup) Register(method, pattern string, svc Service) {
	r.router.Register(method, pattern, svc)
}

func (r *Router) GET(pattern string, svc Service) {
	r.Register("GET", pattern, svc)
}

func (r *Router) CONNECT(pattern string, svc Service) {
	r.Register("CONNECT", pattern, svc)
}

func (r *Router) DELETE(pattern string, svc Service) {
	r.Register("DELETE", pattern, svc)
}

func (r *Router) HEAD(pattern string, svc Service) {
	r.Register("HEAD", pattern, svc)
}

func (r *Router) OPTIONS(pattern string, svc Service) {
	r.Register("OPTIONS", pattern, svc)
}

func (r *Router) PATCH(pattern string, svc Service) {
	r.Register("PATCH", pattern, svc)
}

func (r *Router) POST(pattern string, svc Service) {
	r.Register("POST", pattern, svc)
}

func (r *Router) PUT(pattern string, svc Service) {
	r.Register("PUT", pattern, svc)
}

func (r *Router) TRACE(pattern string, svc Service) {
	r.Register("TRACE", pattern, svc)
}

func (r *RouterGroup) GET(pattern string, svc Service) {
	r.router.Register("GET", pattern, svc)
}

func (r *RouterGroup) CONNECT(pattern string, svc Service) {
	r.router.Register("CONNECT", pattern, svc)
}

func (r *RouterGroup) DELETE(pattern string, svc Service) {
	r.router.Register("DELETE", pattern, svc)
}

func (r *RouterGroup) HEAD(pattern string, svc Service) {
	r.router.Register("HEAD", pattern, svc)
}

func (r *RouterGroup) OPTIONS(pattern string, svc Service) {
	r.router.Register("OPTIONS", pattern, svc)
}

func (r *RouterGroup) PATCH(pattern string, svc Service) {
	r.router.Register("PATCH", pattern, svc)
}

func (r *RouterGroup) POST(pattern string, svc Service) {
	r.router.Register("POST", pattern, svc)
}

func (r *RouterGroup) PUT(pattern string, svc Service) {
	r.router.Register("PUT", pattern, svc)
}
func (r *RouterGroup) TRACE(pattern string, svc Service) {
	r.router.Register("TRACE", pattern, svc)
}
