package hermes

import (
	"context"
	"crypto/tls"
	"errors"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/Shopify/sarama"
	"gitlab.com/shivam-909/heck"
)

type KafkaContextKey struct{}

const (
	KafkaClusterVersion = "2.1.1"
)

var (
	verbose = true
	kck     = KafkaContextKey{}
)

func SetVerbosity(b bool) {
	verbose = b
}

type ConsumerConfig struct {
	Username            string
	Password            string
	Brokers             []string
	Topics              []string
	Group               string
	Assignor            string
	InitialOffsetOldest bool
	scfg                *sarama.Config
}

func (c *ConsumerConfig) Sarama() *sarama.Config {
	return c.scfg
}

func DefaultSaramaConfig(username, password string) *sarama.Config {
	version, err := sarama.ParseKafkaVersion(KafkaClusterVersion)
	if err != nil {
		log.Fatalf("failed to parse kafka version: %v", err)
	}
	saramaConfig := sarama.NewConfig()
	saramaConfig.Net.SASL.Enable = true
	saramaConfig.Net.SASL.Handshake = true
	saramaConfig.Version = version
	saramaConfig.Net.SASL.Mechanism = sarama.SASLTypePlaintext
	saramaConfig.Net.SASL.User = username
	saramaConfig.Net.SASL.Password = password
	saramaConfig.Net.TLS.Enable = true
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		ClientAuth:         0,
	}
	saramaConfig.Net.TLS.Config = tlsConfig

	return saramaConfig
}

func (c *ConsumerConfig) valid() bool {
	if len(c.Brokers) == 0 {
		return false
	}

	if len(c.Topics) == 0 {
		return false
	}

	if c.Group == "" || c.Username == "" || c.Password == "" {
		return false
	}

	return true
}

type ConsumerHandler func(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error

type Consumer struct {
	ready    chan bool
	handler  ConsumerHandler
	cg       sarama.ConsumerGroup
	isPaused *bool

	Configuration *ConsumerConfig
}

func NewConsumer(cfg *ConsumerConfig, handler ConsumerHandler) (*Consumer, error) {
	if !cfg.valid() {
		return nil, errors.New("invalid configuration")
	}
	if verbose {
		sarama.Logger = log.New(os.Stdout, "[sarama] :: ", log.LstdFlags)
	}

	saramaConfig := DefaultSaramaConfig(cfg.Username, cfg.Password)

	switch cfg.Assignor {
	case "sticky":
		saramaConfig.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategySticky
	case "roundrobin":
		saramaConfig.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRoundRobin
	case "range":
		saramaConfig.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRange
	default:
		log.Panicf("unrecognized consumer group partition assignor: %s", cfg.Assignor)
	}

	if cfg.InitialOffsetOldest {
		saramaConfig.Consumer.Offsets.Initial = sarama.OffsetOldest
	}

	cfg.scfg = saramaConfig

	consumer := &Consumer{
		ready:    make(chan bool),
		handler:  handler,
		isPaused: func(v bool) *bool { return &v }(false),

		Configuration: cfg,
	}

	return consumer, nil
}

func (c *Consumer) Setup(sarama.ConsumerGroupSession) error {
	close(c.ready)
	return nil
}

func (c *Consumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *Consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	return c.handler(session, claim)
}

func (c *Consumer) StartListening(ctxx context.Context) error {

	cnsmr, err := sarama.NewConsumerGroup(c.Configuration.Brokers, c.Configuration.Group, c.Configuration.scfg)
	if err != nil {
		return heck.Wrap("failed to create new consumer group", err)
	}

	c.cg = cnsmr

	client, err := sarama.NewClient(c.Configuration.Brokers, c.Configuration.scfg)
	if err != nil {
		return heck.Wrap("failed to create new client", err)
	}

	if mismatch, err := TopicMismatch(client, c.Configuration.Topics); err != nil {
		return heck.Wrap("failed to check for topic mismatch", err)
	} else if mismatch {
		cadmin, err := sarama.NewClusterAdmin(c.Configuration.Brokers, c.Configuration.scfg)
		if err != nil {
			return heck.Wrap("failed to create new cluster", err)
		}

		err = CreateTopicsIfNotExist(cadmin, client, c.Configuration.Topics)
		if err != nil {
			return heck.Wrap("failed to create topics if not exist", err)
		}
	}

	keepRunning := true

	ctx, cancel := context.WithCancel(context.WithValue(ctxx, kck, nil))

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			if err := c.cg.Consume(ctx, c.Configuration.Topics, c); err != nil {
				log.Panicf(heck.Wrap("failed to consume message: %v", err).Error())
				return
			}

			if ctx.Err() != nil {
				return
			}

			c.ready = make(chan bool)
		}
	}()

	<-c.ready

	sigusr1 := make(chan os.Signal, 1)
	signal.Notify(sigusr1, syscall.SIGUSR1)

	sigterm := make(chan os.Signal, 1)
	signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)

	for keepRunning {
		select {
		case <-ctx.Done():
			log.Println("terminating: context cancelled")
			keepRunning = false
		case <-sigterm:
			log.Println("terminating: via signal")
			keepRunning = false
		case <-sigusr1:
			c.ToggleConsumption()
		}
	}
	cancel()
	wg.Wait()
	if err = client.Close(); err != nil {
		log.Panicf("error closing client: %v", err)
		return err
	}

	return nil
}

func (c *Consumer) ToggleConsumption() {
	if *c.isPaused {
		c.cg.ResumeAll()
		log.Println("consumer has resumed")
	} else {
		c.cg.PauseAll()
		log.Println("consumer has paused")
	}

	*c.isPaused = !*c.isPaused
}

func TopicMismatch(cl sarama.Client, topics []string) (bool, error) {
	t, err := cl.Topics()
	if err != nil {
		return false, heck.Wrap("failed to get existing topics", err)
	}

	for _, topic := range topics {
		if !topicExists(t, topic) {
			return true, nil
		}
	}

	return false, nil

}

func CreateTopicsIfNotExist(ca sarama.ClusterAdmin, cl sarama.Client, topics []string) error {
	t, err := cl.Topics()
	if err != nil {
		return heck.Wrap("failed to get existing topics", err)
	}

	for _, topic := range topics {
		if topicExists(t, topic) {
			continue
		}

		err := ca.CreateTopic(topic, nil, false)
		if err != nil {
			return heck.Wrap("failed to create new topic", err)
		}
	}

	return nil
}

func topicExists(src []string, target string) bool {
	for _, s := range src {
		if s == target {
			return true
		}
	}

	return false
}
