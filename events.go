package hermes

import (
	"encoding/json"

	"gitlab.com/shivam-909/heck"
)

type EventType int

type Event struct {
	Type    EventType   `json:"type"`
	Message interface{} `json:"message"`
}

func (e *Event) Encode() (string, error) {
	b2, err := json.Marshal(e)
	if err != nil {
		return "", heck.Wrap("failed to marshal event", err)
	}

	return string(b2), nil
}

func GetEventType(value []byte) (EventType, error) {
	tmp := &struct {
		T EventType `json:"type"`
	}{}

	err := json.Unmarshal(value, tmp)
	if err != nil {
		return 0, err
	}

	return tmp.T, nil
}
